import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:renewit/providers/products_provider.dart';
import 'package:renewit/screens/product_form.dart';

import '../providers/product.dart';

class UserProductItem extends StatelessWidget {
  final Product product;

  UserProductItem(this.product);

  @override
  Widget build(BuildContext context) {
    final _productProvider =
        Provider.of<ProductsProvider>(context, listen: false);

    return Column(
      children: <Widget>[
        ListTile(
          leading: CircleAvatar(
            backgroundImage: NetworkImage(product.imageUrl),
          ),
          title: Text(product.title),
          trailing: Container(
            width: 100.0,
            child: Row(
              children: <Widget>[
                IconButton(
                    icon: Icon(
                      Icons.edit,
                      color: Theme.of(context).primaryColor,
                    ),
                    onPressed: () {
                      Navigator.of(context).pushNamed(
                          ProductFormScreen.routeName,
                          arguments: product);
                    }),
                IconButton(
                    icon: Icon(
                      Icons.delete,
                      color: Theme.of(context).errorColor,
                    ),
                    onPressed: () {
                      showDialog(
                        context: context,
                        builder: (ctx) => AlertDialog(
                          title: Text('Deleting a product'),
                          content: Text(
                              'Are you sure you want to delete the product?'),
                          actions: <Widget>[
                            FlatButton(
                              onPressed: () => Navigator.of(context).pop(true),
                              child: Text('YES'),
                            ),
                            FlatButton(
                              onPressed: () => Navigator.of(context).pop(false),
                              child: Text('NO'),
                            ),
                          ],
                        ),
                      ).then((result) {
                        if (result) {
                          _productProvider.removeProduct(product.id);
                        }
                      });
                    }),
              ],
            ),
          ),
        ),
        Divider(),
      ],
    );
  }
}
