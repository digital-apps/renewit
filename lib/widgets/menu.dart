import 'package:flutter/material.dart';

import '../screens/orders_screen.dart';
import '../screens/user_products.dart';

class Menu extends StatelessWidget {
  const Menu({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          AppBar(
            centerTitle: true,
            automaticallyImplyLeading: false,
            //leading: Container(),
            title: Text(
              'Main Menu',
            ),
          ),
          ListTile(
            leading: Icon(Icons.shopping_cart),
            title: Text(
              "Shop",
              style: Theme.of(context).textTheme.headline6,
            ),
            onTap: () => Navigator.of(context).pushReplacementNamed('/'),
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.payment),
            title: Text(
              "Orders",
              style: Theme.of(context).textTheme.headline6,
            ),
            onTap: () =>
                Navigator.of(context).pushNamed(OrdersScreen.routeName),
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.edit),
            title: Text(
              "Manage Products",
              style: Theme.of(context).textTheme.headline6,
            ),
            onTap: () =>
                Navigator.of(context).pushNamed(UserProductsScreen.routeName),
          ),
        ],
      ),
    );
  }
}
