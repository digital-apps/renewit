import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:renewit/providers/products_provider.dart';

import '../providers/product.dart';

class ProductFormScreen extends StatefulWidget {
  static const routeName = '/product-form';

  @override
  _ProductFormScreenState createState() => _ProductFormScreenState();
}

class _ProductFormScreenState extends State<ProductFormScreen> {
  final _priceNode = FocusNode();
  final _descriptionNode = FocusNode();
  final _imageUrlNode = FocusNode();

  final _imageUrlController = TextEditingController();

  final _form = GlobalKey<FormState>();

  Product _product;
  bool _initDone = false;

  void _updateImageUrl() {
    if (!_imageUrlNode.hasFocus) {
      // Trick for updating the image preview
      setState(() {});
    }
  }

  @override
  void initState() {
    _imageUrlController.addListener(_updateImageUrl);

    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (!_initDone) {
      _product = ModalRoute.of(context).settings.arguments as Product;

      if (_product == null) {
        _product = Product(
            id: null, title: '', description: '', imageUrl: '', price: 0);
      } else {
        _imageUrlController.text = _product.imageUrl;
      }

      _initDone = true;
    }
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _priceNode.dispose();
    _descriptionNode.dispose();
    _imageUrlNode.dispose();

    _imageUrlController.removeListener(_updateImageUrl);
    _imageUrlController.dispose();

    super.dispose();
  }

  void _saveForm() {
    final productProvider =
        Provider.of<ProductsProvider>(context, listen: false);
    final isValid = _form.currentState.validate();
    if (!isValid) {
      return;
    }

    _form.currentState.save();
    if (_product.id == null) {
      productProvider.addProduct(_product);
    } else {
      productProvider.updateProduct(_product);
    }
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Product Form'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.save),
            onPressed: _saveForm,
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _form,
          child: ListView(
            children: <Widget>[
              TextFormField(
                initialValue: _product.title,
                decoration: InputDecoration(labelText: 'Title'),
                textInputAction: TextInputAction.next,
                onFieldSubmitted: (text) =>
                    FocusScope.of(context).requestFocus(_priceNode),
                validator: (value) {
                  if (value.isEmpty) {
                    return "Please enter a title";
                  } else
                    // validation OK
                    return null;
                },
                onSaved: (value) => _product = Product(
                  id: _product.id,
                  title: value,
                  description: _product.description,
                  price: _product.price,
                  imageUrl: _product.imageUrl,
                ),
              ),
              TextFormField(
                initialValue:
                    _product.price > 0 ? _product.price.toString() : '',
                decoration: InputDecoration(labelText: 'Price'),
                textInputAction: TextInputAction.next,
                keyboardType: TextInputType.number,
                focusNode: _priceNode,
                onFieldSubmitted: (text) =>
                    FocusScope.of(context).requestFocus(_descriptionNode),
                validator: (value) {
                  var result;

                  if (value.isEmpty) {
                    result = 'Please enter a price';
                  } else if (double.tryParse(value) == null) {
                    result = 'Please enter a valid price';
                  } else if (double.parse(value) < 0) {
                    result = 'Please enter a number greater than zero';
                  }

                  return result;
                },
                onSaved: (value) => _product = Product(
                  id: _product.id,
                  title: _product.title,
                  description: _product.description,
                  price: double.parse(value),
                  imageUrl: _product.imageUrl,
                ),
              ),
              TextFormField(
                initialValue: _product.description,
                decoration: InputDecoration(labelText: 'Description'),
                textInputAction: TextInputAction.next,
                maxLines: 3,
                keyboardType: TextInputType.multiline,
                focusNode: _descriptionNode,
                onFieldSubmitted: (text) =>
                    FocusScope.of(context).requestFocus(_imageUrlNode),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter a description';
                  } else if (value.length < 10) {
                    return 'Should be at least 10 characters long';
                  }
                  // validation OK
                  return null;
                },
                onSaved: (value) => _product = Product(
                  id: _product.id,
                  title: _product.title,
                  description: value,
                  price: _product.price,
                  imageUrl: _product.imageUrl,
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Container(
                    height: 100.0,
                    width: 100.0,
                    padding: EdgeInsets.symmetric(horizontal: 8.0),
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.grey[400],
                      ),
                    ),
                    child: _imageUrlController.text.isEmpty
                        ? Text(
                            'Enter a URL',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.grey[700],
                            ),
                          )
                        : FittedBox(
                            child: Image.network(
                              _imageUrlController.text,
                              fit: BoxFit.cover,
                            ),
                          ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: TextFormField(
                        decoration: InputDecoration(labelText: 'Image URL'),
                        keyboardType: TextInputType.url,
                        textInputAction: TextInputAction.done,
                        focusNode: _imageUrlNode,
                        controller: _imageUrlController,
                        onFieldSubmitted: (_) => _saveForm(),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter an image URL';
                          } else if (!value.startsWith('http')) {
                            return 'Please enter a valid URL';
                          } else if (!value.endsWith('.png') &&
                              !value.endsWith('.jpg') &&
                              !value.endsWith('.jpeg')) {
                            return 'Please enter a valid image URL';
                          }
                          // validation OK
                          return null;
                        },
                        onSaved: (value) => _product = Product(
                          id: _product.id,
                          title: _product.title,
                          description: _product.description,
                          price: _product.price,
                          imageUrl: value,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
