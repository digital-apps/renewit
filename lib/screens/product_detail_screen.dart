import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:renewit/providers/products_provider.dart';

class ProductDetailScreen extends StatelessWidget {
  static const String routeName = '/productDetails';

  @override
  Widget build(BuildContext context) {
    final routeArgs =
        ModalRoute.of(context).settings.arguments as Map<String, Object>;
    final productId = routeArgs['productId'];
    final productProvider = Provider.of<ProductsProvider>(
      context,
      listen: false, // sets this widget as a "passive" listeners
    );
    final product = productProvider.getProductById(productId);

    return Scaffold(
      appBar: AppBar(
        title: Text(product.title),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              height: 400,
              width: double.infinity,
              child: Image.network(
                product.imageUrl,
                fit: BoxFit.cover,
              ),
              //alignment: Alignment.center,
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              '\$ ${product.price}',
              style: TextStyle(
                color: Colors.grey,
                fontSize: 20.0,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: 10.0,
              ),
              width: double.infinity,
              child: Text(
                product.description,
                textAlign: TextAlign.center,
                softWrap: true,
              ),
            )
          ],
        ),
      ),

      // Center(
      //     child: Text(
      //   '${product.price}',
      // )),
    );
  }
}
