import 'package:flutter/widgets.dart';

class CartItem {
  final String id;
  final String title;
  final int quatity;
  final double price;

  CartItem({
    @required this.id,
    @required this.title,
    @required this.quatity,
    @required this.price,
  });
}

class CartProvider with ChangeNotifier {
  Map<String, CartItem> _items = {};

  Map<String, CartItem> get items {
    return {..._items};
  }

  void addItem(String productId, double price, String title) {
    if (_items.containsKey(productId)) {
      _items.update(
          productId,
          (existingCartItem) => CartItem(
              id: existingCartItem.id,
              title: existingCartItem.title,
              quatity: existingCartItem.quatity + 1,
              price: existingCartItem.price));
    } else {
      _items.putIfAbsent(
          productId,
          () => CartItem(
                id: DateTime.now().toString(),
                title: title,
                price: price,
                quatity: 1,
              ));
    }
    notifyListeners();
  }

  void removeLastItem(String productId) {
    if (!_items.containsKey(productId)) {
      return;
    }

    if (_items[productId].quatity > 1) {
      _items.update(
          productId,
          (cartItem) => CartItem(
                id: cartItem.id,
                title: cartItem.title,
                quatity: cartItem.quatity - 1,
                price: cartItem.price,
              ));
    } else {
      _items.remove(productId);
    }

    notifyListeners();
  }

  /// Get count of items by product type
  int get itemCount {
    return _items.length;
  }

  /// Calculates total
  double get totalAmount {
    double total = 0.0;
    _items.forEach((key, cartItem) {
      total += cartItem.price * cartItem.quatity;
    });

    return total;
  }

  /// Remove Item
  void removeItem(String productId) {
    _items.remove(productId);
    notifyListeners();
  }

  /// Clear cart
  void clearCart() {
    _items.clear();
    notifyListeners();
  }
}
