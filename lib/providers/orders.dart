import 'package:flutter/foundation.dart';
import './cart_provider.dart';

class OrderItem {
  final String id;
  final double amount;
  final List<CartItem> products;
  final DateTime dateTime;

  OrderItem({
    @required this.id,
    @required this.amount,
    @required this.products,
    @required this.dateTime,
  });
}

class OrdersProvider with ChangeNotifier {
  List<OrderItem> _orders = [];

  /// get orders
  List<OrderItem> get orders {
    return [..._orders];
  }

  /// Adding an order
  void addOrder(List<CartItem> cartItems, double total) {
    _orders.insert(
        0,
        OrderItem(
          id: DateTime.now().toString(),
          amount: total,
          products: cartItems,
          dateTime: DateTime.now(),
        ));

    notifyListeners();
  }
}
