import 'package:flutter/material.dart';

class AppTheme {
  /// Define the main theme properties
  static ThemeData mainTheme() {
    return ThemeData(
      primarySwatch: Colors.orange,
      accentColor: Colors.teal[600],
      fontFamily: 'TitilliumWeb',
      
      // This makes the visual density adapt to the platform that you run
      // the app on. For desktop platforms, the controls will be smaller and
      // closer together (more dense) than on mobile platforms.
      visualDensity: VisualDensity.adaptivePlatformDensity,
    );
  }
}
